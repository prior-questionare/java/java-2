## Parallel processing

### Intro

Your batch module is about read a file and send a record to another service to be inserted into database but they take 1 sec to inserted
and your batch module send a record one by one its take 100 sec to be done because its sequencial programming!!!

So your senior assigned you to make it faster!!

hint. use a Flux stream and make it parallel strom a many thread to help this out!!!

### Start environment with
```sh
sudo ./build-assignment.sh
```
### Stop environment with
```sh
sudo ./stop-assignment.sh
```

### Where to fix code
Code to be fix is in batch folder

### Step to reproduce a problem
- start a environment with a giving command
- see a result by monitor a logs
- you can see logs by
```sh
sudo docker logs -f assignment_batch
```
```sh
calling api with data North America,Mexico,Personal Care,Offline,L,2/17/2012,430915820,3/20/2012,6422,81.73,56.67,524870.06,363934.74,160935.32
json = North America,Mexico,Personal Care,Offline,L,2/17/2012,430915820,3/20/2012,6422,81.73,56.67,524870.06,363934.74,160935.32
responseBody = insert ok
calling api with data Sub-Saharan Africa,Sao Tome and Principe,Beverages,Offline,C,1/16/2011,180283772,1/21/2011,8829,47.45,31.79,418936.05,280673.91,138262.14
json = Sub-Saharan Africa,Sao Tome and Principe,Beverages,Offline,C,1/16/2011,180283772,1/21/2011,8829,47.45,31.79,418936.05,280673.91,138262.14
responseBody = insert ok
calling api with data Sub-Saharan Africa,The Gambia,Baby Food,Offline,M,2/3/2014,494747245,3/20/2014,5559,255.28,159.42,1419101.52,886215.78,532885.74
json = Sub-Saharan Africa,The Gambia,Baby Food,Offline,M,2/3/2014,494747245,3/20/2014,5559,255.28,159.42,1419101.52,886215.78,532885.74
responseBody = insert ok
calling api with data Middle East and North Africa,Kuwait,Fruits,Online,M,4/30/2012,513417565,5/18/2012,522,9.33,6.92,4870.26,3612.24,1258.02
json = Middle East and North Africa,Kuwait,Fruits,Online,M,4/30/2012,513417565,5/18/2012,522,9.33,6.92,4870.26,3612.24,1258.02
responseBody = insert ok
calling api with data Europe,Slovenia,Beverages,Offline,C,10/23/2016,345718562,11/25/2016,4660,47.45,31.79,221117.00,148141.40,72975.60
json = Europe,Slovenia,Beverages,Offline,C,10/23/2016,345718562,11/25/2016,4660,47.45,31.79,221117.00,148141.40,72975.60
responseBody = insert ok
calling api with data Sub-Saharan Africa,Sierra Leone,Office Supplies,Offline,H,12/6/2016,621386563,12/14/2016,948,651.21,524.96,617347.08,497662.08,119685.00
json = Sub-Saharan Africa,Sierra Leone,Office Supplies,Offline,H,12/6/2016,621386563,12/14/2016,948,651.21,524.96,617347.08,497662.08,119685.00
responseBody = insert ok
calling api with data Australia and Oceania,Australia,Beverages,Offline,H,7/7/2014,240470397,7/11/2014,9389,47.45,31.79,445508.05,298476.31,147031.74
json = Australia and Oceania,Australia,Beverages,Offline,H,7/7/2014,240470397,7/11/2014,9389,47.45,31.79,445508.05,298476.31,147031.74
responseBody = insert ok
calling api with data Middle East and North Africa,Azerbaijan,Office Supplies,Online,M,6/13/2012,423331391,7/24/2012,2021,651.21,524.96,1316095.41,1060944.16,255151.25
json = Middle East and North Africa,Azerbaijan,Office Supplies,Online,M,6/13/2012,423331391,7/24/2012,2021,651.21,524.96,1316095.41,1060944.16,255151.25
responseBody = insert ok
calling api with data Europe,Romania,Cosmetics,Online,H,11/26/2010,660643374,12/25/2010,7910,437.20,263.33,3458252.00,2082940.30,1375311.70
json = Europe,Romania,Cosmetics,Online,H,11/26/2010,660643374,12/25/2010,7910,437.20,263.33,3458252.00,2082940.30,1375311.70
responseBody = insert ok
calling api with data Central America and the Caribbean,Nicaragua,Beverages,Offline,C,2/8/2011,963392674,3/21/2011,8156,47.45,31.79,387002.20,259279.24,127722.96
json = Central America and the Caribbean,Nicaragua,Beverages,Offline,C,2/8/2011,963392674,3/21/2011,8156,47.45,31.79,387002.20,259279.24,127722.96
responseBody = insert ok
calling api with data Sub-Saharan Africa,Mali,Clothes,Online,M,7/26/2011,512878119,9/3/2011,888,109.28,35.84,97040.64,31825.92,65214.72
json = Sub-Saharan Africa,Mali,Clothes,Online,M,7/26/2011,512878119,9/3/2011,888,109.28,35.84,97040.64,31825.92,65214.72
responseBody = insert ok
calling api with data Asia,Malaysia,Fruits,Offline,L,11/11/2011,810711038,12/28/2011,6267,9.33,6.92,58471.11,43367.64,15103.47
json = Asia,Malaysia,Fruits,Offline,L,11/11/2011,810711038,12/28/2011,6267,9.33,6.92,58471.11,43367.64,15103.47
responseBody = insert ok
calling api with data Sub-Saharan Africa,Sierra Leone,Vegetables,Offline,C,6/1/2016,728815257,6/29/2016,1485,154.06,90.93,228779.10,135031.05,93748.05
json = Sub-Saharan Africa,Sierra Leone,Vegetables,Offline,C,6/1/2016,728815257,6/29/2016,1485,154.06,90.93,228779.10,135031.05,93748.05
responseBody = insert ok
calling api with data North America,Mexico,Personal Care,Offline,M,7/30/2015,559427106,8/8/2015,5767,81.73,56.67,471336.91,326815.89,144521.02
json = North America,Mexico,Personal Care,Offline,M,7/30/2015,559427106,8/8/2015,5767,81.73,56.67,471336.91,326815.89,144521.02
responseBody = insert ok
calling api with data Sub-Saharan Africa,Mozambique,Household,Offline,L,2/10/2012,665095412,2/15/2012,5367,668.27,502.54,3586605.09,2697132.18,889472.91
json = Sub-Saharan Africa,Mozambique,Household,Offline,L,2/10/2012,665095412,2/15/2012,5367,668.27,502.54,3586605.09,2697132.18,889472.91
responseBody = insert ok
minute 1
seconds 101
milliseconds 101951
2021-09-27 11:04:43.760  INFO 1 --- [           main] o.s.b.web.embedded.netty.NettyWebServer  : Netty started on port 8080
2021-09-27 11:04:43.778  INFO 1 --- [           main] t.c.p.p.p.ParallelProcessingApplication  : Started ParallelProcessingApplication in 104.342 seconds (JVM running for 104.931)

```

### Expected Result
```sh
make it faster than 10x.xx seconds
```
