package th.co.priorsolution.psgame.parallelprocessing.schedule;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Component
public class ScheduleTask {

    @Value("${app.csv}")
    private String appCsvFile;

    @Value("${app.api.insert}")
    private String appApiInsert;

    @PostConstruct
    public void runATask(){

        List<String> records = this.readFile(this.appCsvFile);

        LocalDateTime start = LocalDateTime.now();

        for (String d:records) {
            System.out.println("calling api with data "+ d);
            this.callApi(this.appApiInsert, d);
        }

        LocalDateTime end = LocalDateTime.now();
        long minutes = start.until( end, ChronoUnit.MINUTES );
        long seconds = start.until( end, ChronoUnit.SECONDS );
        long millisec = start.until( end, ChronoUnit.MILLIS );
        System.out.println("minute "+minutes);
        System.out.println("seconds "+seconds);
        System.out.println("milliseconds "+millisec);

    }

    private List<String> readFile(String fileLocation) {

        List<String> result = new ArrayList<>();

        try {
            File reconcileFile = new File(fileLocation);
            BufferedReader br = new BufferedReader(new FileReader(reconcileFile));
            String st;
            while ((st = br.readLine()) != null) {
                System.out.println("read line "+st);

                result.add(st);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String callApi(String surl, String data) {
        String result = "";
        try {
            URL url = new URL(surl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");

            //make requestObject

            //set header
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Content-type", "application/json");
            //transform to string
            String json = data;
            System.out.println("json = " + json);
            byte[] input = json.getBytes();
            con.setRequestProperty("Content-Length", String.valueOf(input.length));
            //send to api
            OutputStream os = con.getOutputStream();
            os.write(input);
            InputStream ips = con.getInputStream();
            InputStreamReader ipsR = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsR);
            StringBuilder response = new StringBuilder();
            if (HttpURLConnection.HTTP_OK != con.getResponseCode()) {
                response.append(br.lines().collect(Collectors.joining()));
            } else {
                response.append(br.lines().collect(Collectors.joining()));
            }
            System.out.println("responseBody = " + response);
            result = response.toString();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;

    }
}
