docker build -f MockDockerfile . -t assignment_mock
docker-compose -f docker-asset/mock/mock-compose.yml up -d
docker build -f BatchDockerfile . -t assignment_batch
docker-compose -f docker-asset/batch/batch-compose.yml up -d

