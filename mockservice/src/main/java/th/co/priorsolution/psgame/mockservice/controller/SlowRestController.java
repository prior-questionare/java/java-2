package th.co.priorsolution.psgame.mockservice.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/slow/api")
public class SlowRestController {

    @PostMapping("/insert2sec")
    public String slowService(@RequestBody String requestBody){
        System.out.println("inserting "+requestBody);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "insert ok";
    }
}
